package encapsulation;

import java.util.Arrays;
import java.util.*;


// public class exception {

// 	public static void main(String[] args) {
//	  int a=10,b=0;
//	  int c=0;
//	  try {
//		  c=a/b;
//	  }
//	  catch(Exception e) {
//		  System.out.println("error occurred");
//	  }
//	  
//   System.out.println(c);
		
//		String method ----- 
		
//		String s="Guna";
//		String s2="Guna";
//        String s4=new String("Guna");
//        System.out.println(s==s4);
//        System.out.println(s.equals(s4));
//        int n[]=new int[5];
//        n[0]=2;
//        n[1]=5;
//        n[2]=1;
//        n[3]=10;
//        Arrays.sort(n);
//        System.out.println(Arrays.toString(n));
//        //int[][] n1=new int[3][3];
//        int num[][]= {{1,2,3},{5,6,7}};
//        System.out.println(Arrays.deepToString(num));
        
        
//        ------arraylist -----
        
       
//        ArrayList<String> list=new ArrayList<String>();//Creating arraylist  
//        list.add("Ravi");//Adding object in arraylist  
//        list.add("Vijay");  
//        list.add("Ravi");  
//        list.add("Ajay");  
//        //Traversing list through Iterator  
//        Iterator itr=list.iterator();  
//        while(itr.hasNext()){  
//        System.out.println(itr.next());  
//        }  
//        
        
//	-------	linkedList   ----
//        LinkedList<String> list=new LinkedList<String>();//Creating Linkedlist  
//        list.add("Guna");//Adding object in Linkedlist 
//        list.add("priya");  
//        list.add("Ravi");  
//        list.add("Ajay");  
//        //Traversing list through Iterator  
//        Iterator itr=list.iterator();  
//        while(itr.hasNext()){  
//        System.out.println(itr.next());  
		
//		----vector list --
		
//		Vector<String> v=new Vector<String>();  
//		v.add("Ayush");  
//		v.add("Amit");  
//		v.add("Ashish");  
//		v.add("Garima");  
//		Iterator<String> itr=v.iterator();  
//		while(itr.hasNext()){  
//		System.out.println(itr.next());  
//		
//		
//        }  
		
//		---stack List-----
//        
		// Stack<String> stack = new Stack<String>();  
		// stack.push("Ayush");  
		// stack.push("Garvit");  
		// stack.push("Amit");  
		// stack.push("Ashish");  
		// stack.push("Garima");  
		// stack.pop();  
		// Iterator<String> itr=stack.iterator();  
		// while(itr.hasNext()){  
		// System.out.println(itr.next());  

//        class Bike1{  
//        Bike1(){System.out.println("Bike is created");}    
//        public static void main(String args[]){  
//        //calling a default constructor  
//        Bike1 b=new Bike1();  
// }  
// }  

// 		}  
// 	}

// }



// class Student5{  
//     int id;  
//     String name;  
//     int age;  
//     Student5(int i,String n){  
//     id = i;  
//     name = n;  
//     }  
//     Student5(int i,String n,int a){  
//     id = i;  
//     name = n;  
//     age=a;  
//     }  
//     void display(){System.out.println(id+" "+name+" "+age);}  
//     public static void main(String args[]){  
//     Student5 s1 = new Student5(111,"Karan");  
//     Student5 s2 = new Student5(222,"Aryan",25);  
//     s1.display();  
//     s2.display();  
//    }  
// }  

class Student7{  
    int id;  
    String name;  
    Student7(int i,String n){  
    id = i;  
    name = n;  
    }  
    Student7(){}  
    void display(){System.out.println(id+" "+name);}  
   
    public static void main(String args[]){  
    Student7 s1 = new Student7(111,"Karan");  
    Student7 s2 = new Student7();  
    s2.id=s1.id;  
    s2.name=s1.name;  
    s1.display();  
    s2.display();  
   }  
}  